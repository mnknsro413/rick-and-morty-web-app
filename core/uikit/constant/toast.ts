export enum ToastStatus {
    Success = "success",
    Error = "error",
    Info = "info",
    Warning = "warning",
}

export enum ToastPosition {
    Top = "top",
    TopRight = "top-right",
    TopLeft = "top-left",
}