import React, { createContext, useContext, useState, ReactNode, ReactElement } from "react"
import styles from "../tooltip/Toast.module.scss"
import { ToastStatus, ToastPosition } from "../../constant/toast"
import { ToastContext, ToastMessage, ToastProviderProps } from "../../type/toast.type"

export const ToastProvider: React.FunctionComponent<ToastProviderProps> = ({
  children,
}: ToastProviderProps): ReactElement => {
  const [toasts, setToasts] = useState<ToastMessage[]>([])

  const showToast = (message: ToastMessage): void => {
    setToasts((prevToasts) => [...prevToasts, message])
    setTimeout(() => {
      setToasts((prevToasts) => prevToasts.slice(1))
    }, message.duration || 2000)
  }

  return (
    <ToastContext.Provider value={{ showToast }}>
      {children}
      <div className={styles.toastsContainer}>
        {toasts.map((toast, index) => (
          <div
            key={index}
            className={`${styles.toast} ${styles[toast.status]} ${
              styles[toast.position || ToastPosition.Top]
            } ${styles.visible}`}
            onClick={() => setToasts((prevToasts) => prevToasts.slice(1))}
          >
            {toast.title && <strong>{toast.title}</strong>}
            <p>{toast.description}</p>
          </div>
        ))}
      </div>
    </ToastContext.Provider>
  )
}

export const useToast = (): ((message: ToastMessage) => void) => {
  const context = useContext(ToastContext)

  if (!context) {
    throw new Error("useToast must be used within a ToastProvider")
  }

  return context.showToast
}
