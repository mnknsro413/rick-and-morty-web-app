import { ToastPosition, ToastStatus } from "@/module/app-characters/constant"
import { ReactNode, createContext } from "react"

export interface ToastContextProps {
    showToast: (message: ToastMessage) => void
}

export interface ToastMessage {
    title?: string
    description?: string
    status: ToastStatus
    duration?: number
    isClosable?: boolean
    position?: ToastPosition
}

export const ToastContext = createContext<ToastContextProps | undefined>(undefined)

export interface ToastProviderProps {
    children: ReactNode
}