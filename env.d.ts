/// <reference types="vite/client" />

interface ImportMetaEnv {
  readonly NEXT_BASE_PATH: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
