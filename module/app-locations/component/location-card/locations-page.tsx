import React, { FC } from "react"
import style from "./style/LocationCard.module.scss"
import { FaArrowRight } from "react-icons/fa"
import { LocationsCardProps } from "@/module/app-locations/component/location-card/location-card.type"

const locationsPage: FC<LocationsCardProps> = (props) => {
  const { name, type, dimension, residentsCount } = props

  return (
    <div className={style.container}>
      <div className={style.action}>
        <FaArrowRight />
      </div>
      <div className={style.content}>
        <div className={style.info}>
          <h4>Name</h4>
          <b>{name}</b>
        </div>
        <div className={style.info}>
          <h4>Type</h4>
          <b>{type}</b>
        </div>
        <div className={style.info}>
          <h4>Dimension</h4>
          <b>{dimension == "unknown" ? "-" : dimension}</b>
        </div>
        <div className={style.info}>
          <h4>Residents Count</h4>
          <b>{residentsCount}</b>
        </div>
      </div>
    </div>
  )
}

export default locationsPage
