import { PropsWithChildren } from "react"

export type LocationsCardProps = PropsWithChildren & {
    name: string
    type: string
    dimension: string
    residentsCount: number
}
