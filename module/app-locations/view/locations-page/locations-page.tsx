import React, { useState } from "react"
import { useQuery } from "react-query"
import { fetchData } from "@/package/service/api"
import MainHeader from "@/package/layouts/main-header"
import LocationCard from "../../component/location-card"
import styles from "../../asset/style/Locations.module.scss"
import Pagination from "@/package/components/pagination"
import { ResultData } from "../../types"
import Link from "next/link"
import LocationCardSkeleton from "@/package/components/skeleton/location-card-skeleton"
import { useToast } from "@/core/uikit/components/tooltip/useToast"
import { ToastPosition, ToastStatus } from "@/core/uikit/constant/toast"
import { useRouter } from "next/navigation"

const LocationsPageView: React.FC = () => {
  const [currentPage, setCurrentPage] = useState(1)
  const showToast = useToast()
  const router = useRouter()

  const {
    data: locations,
    isLoading,
    refetch,
  } = useQuery(["locations", currentPage], () => fetchData(`location?page=${currentPage}`))

  const onPageChange = (page: number) => {
    setCurrentPage(page)
    refetch()
  }

  const extractIdsFromUrls = (urls: string[]): number[] => {
    return urls.map((url) => {
      const urlParts = url.split("/")
      return parseInt(urlParts[urlParts.length - 1], 10)
    })
  }

  return (
    <>
      <header className={styles.welcome}></header>
      <MainHeader />

      <main className={styles.content}>
        {isLoading ? (
          <LocationCardSkeleton />
        ) : (
          locations?.results.map((location: ResultData) => (
            <div
              onClick={() => {
                if (location.residents.length > 0) {
                  router.push(`/characters/${extractIdsFromUrls(location.residents)}`)
                } else {
                  showToast({
                    description: "There are no characters on this planet!",
                    status: ToastStatus.Info,
                    position: ToastPosition.TopRight,
                    duration: 2000,
                    isClosable: true,
                  })
                }
              }}
              style={{ textDecoration: "none" }}
              key={location.id + 1}
            >
              <LocationCard
                name={location.name}
                type={location.type}
                dimension={location.dimension}
                residentsCount={location.residents.length}
              />
            </div>
          ))
        )}
      </main>

      <div style={{ position: "relative", zIndex: 50 }}>
        <Pagination
          totalPageCount={locations?.info.pages || 1}
          currentPage={currentPage}
          onPageChange={onPageChange}
          position="center"
        />
      </div>
    </>
  )
}

export default LocationsPageView
