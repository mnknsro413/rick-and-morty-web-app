// Interface

export interface InfoData {
  count: number;
  pages: number;
  next: string | null;
  prev: string | null;
}

export interface ResultData {
  id: number
  name: string
  type: string
  dimension: string
  residents: string[]
  url: string
  created: Date
}

export interface LocationsData {
  info: InfoData;
  results: ResultData[];
}

