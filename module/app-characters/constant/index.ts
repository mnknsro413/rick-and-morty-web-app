export enum ToastStatus {
    Success = "success",
    Error = "error",
    Info = "info",
    Warning = "warning",
}

export enum ToastPosition {
    Top = "top",
    TopRight = "top-right",
    TopLeft = "top-left",
}


export enum CharacterStatusText {
    AddedToFavorites = "Added to favorites.",
    RemovedFromFavorites = "Removed from favorites.",
}

interface CharacterStatusColorType {
    [key: string]: string;
}

export const CharacterStatusColor: CharacterStatusColorType = {
    Alive: "#00AB86",
    Dead: "#FF5157",
    Unknown: "#CECECE",
};