import { configureStore } from '@reduxjs/toolkit'
import favoritesSlice from '../redux/slices/favoritesSlice'

export const store = configureStore({
  reducer: {
    favorites: favoritesSlice
  },
  devTools: process.env.NODE_ENV !== 'production'
})

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

