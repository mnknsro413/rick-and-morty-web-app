import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { getFavoritesFromLocalStorage, updateFavoritesInLocalStorage } from '../../utils/use-storage';

const initialState = {
    ids: getFavoritesFromLocalStorage(),
};

const favoritesSlice = createSlice({
    name: "favorites",
    initialState,
    reducers: {
        toggleFavorite: (state, action) => {
            const characterId = action.payload;
            const index = state.ids.indexOf(characterId);

            if (index !== -1) {
                // Remove from favorites
                state.ids.splice(index, 1);
            } else {
                // Add to favorites
                state.ids.push(characterId);
            }

            // Update LocalStorage
            updateFavoritesInLocalStorage(state.ids);
        },
    },
});

export const { toggleFavorite } = favoritesSlice.actions;
export default favoritesSlice.reducer;