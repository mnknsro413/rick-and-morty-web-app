import React, { FC, useEffect } from "react"
import { FavoriteCharactersPageViewProps } from "@/module/app-characters/view/favorite-characters-page/favorite-characters-page.type"
import { fetchData } from "@/package/service/api"
import MainHeader from "@/package/layouts/main-header"
import styles from "../../asset/style/Characters.module.scss"
import { ResultData } from "../../type"
import { HiOutlineArrowCircleLeft } from "react-icons/hi"
import CharacterCard from "../../component/character-card"
import CharacterCardSkeleton from "@/package/components/skeleton/character-card-skeleton"
import { useQuery } from "react-query"
import { useAppSelector } from "../../hooks"
import { useRouter } from "next/navigation"

const FavoriteCharactersPageView: FC<FavoriteCharactersPageViewProps> = () => {
  const router = useRouter()

  const favorites = useAppSelector((state) => state.favorites.ids)

  const {
    data: character,
    isLoading,
    refetch,
  } = useQuery(["character"], () => {
    if (favorites.length > 0) {
      // If there are favorites, fetch the characters
      return fetchData(`character/${favorites.join(",")}`)
    } else {
      // If there are no favorites, return an empty array
      return Promise.resolve([])
    }
  })

  useEffect(() => {
    refetch()
  }, [favorites])

  return (
    <div>
      <header className={styles.header}>
        <MainHeader
          leftAction={
            <div className={styles.backButton} onClick={() => router.back()}>
              <HiOutlineArrowCircleLeft />
            </div>
          }
        />
      </header>

      <main className={styles.content}>
        {isLoading ? (
          <CharacterCardSkeleton />
        ) : (
          <>
            {Array.isArray(character) ? (
              character.length > 0 ? (
                character.map((char: ResultData) => (
                  <CharacterCard character={char} key={char.id} />
                ))
              ) : (
                <p>No favorite characters available.</p>
              )
            ) : // If it's not an array, check if it's an object
            character ? (
              <CharacterCard
                character={character as ResultData}
                key={(character as ResultData).id}
              />
            ) : (
              // If it's not an object either, show the "No favorite characters available." message
              <p>No favorite characters available.</p>
            )}
          </>
        )}
      </main>
    </div>
  )
}

export default FavoriteCharactersPageView
