import { PropsWithChildren } from "react"

export type CharacterPageViewProps = PropsWithChildren &
{
    params: string
}
