"use client"

import React, { FC, useMemo, useState } from "react"
import { fetchData } from "@/package/service/api"
import Slider from "react-slick"
import notFound from "@/package/asset/images/not-found.png"
import MainHeader from "@/package/layouts/main-header"
import Pagination from "@/package/components/pagination"
import styles from "../../asset/style/Characters.module.scss"
import { ResultData } from "../../type"
import Link from "next/link"
import { HiOutlineArrowCircleLeft } from "react-icons/hi"
import { CharacterPageViewProps } from "@/module/app-characters/view/characters-page/characters-page.type"
import CharacterCard from "../../component/character-card"
import CharacterCardSkeleton from "@/package/components/skeleton/character-card-skeleton"
import { useQuery } from "react-query"
import { GrPowerReset, GrFormPrevious } from "react-icons/gr"
import { MdOutlineNavigateNext } from "react-icons/md"
import classNames from "classnames"

const sliderSettings = {
  dots: true,
  infinite: true,
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
}

const CharacterPageView: FC<CharacterPageViewProps> = ({ params }) => {
  const [currentPage, setCurrentPage] = useState(1)
  const [statusFilter, setStatusFilter] = useState<string | null>(null)

  const decodedParams = decodeURIComponent(params)

  const { data: character, isLoading } = useQuery(["character", decodedParams], () =>
    fetchData(`character/${decodedParams}`)
  )
  const itemsPerPage = 8

  const filteredCharacters = useMemo(() => {
    const characterArray = Array.isArray(character) ? character : character ? [character] : []
    return statusFilter
      ? characterArray.filter((char: ResultData) => char.status === statusFilter)
      : characterArray
  }, [character, statusFilter])

  const totalPageCount = useMemo(
    () => Math.ceil(filteredCharacters.length / itemsPerPage),
    [filteredCharacters, itemsPerPage]
  )
  const startIndex = (currentPage - 1) * itemsPerPage
  const endIndex = startIndex + itemsPerPage
  const paginatedCharacters = filteredCharacters.slice(startIndex, endIndex)

  const onPageChange = (page: number) => {
    setCurrentPage(page)
  }

  return (
    <div>
      <header className={styles.header}>
        <MainHeader
          leftAction={
            <Link className={styles.backButton} href="/">
              <HiOutlineArrowCircleLeft />
            </Link>
          }
        />
      </header>

      <div className={styles.filterHeader}>
        <div>
          <b>Filter by status</b>
          <div className={styles.filterContainer}>
            <button
              className={`${styles.filter} ${statusFilter === "Dead" ? styles.active : ""}`}
              onClick={() => setStatusFilter("Dead")}
            >
              <div className={styles.statusContainer}>
                <div className={styles.status}></div>
                <div style={{ textTransform: "capitalize" }}>Dead</div>
              </div>
            </button>
            <button
              className={`${styles.filter} ${statusFilter === "Alive" ? styles.active : ""}`}
              onClick={() => setStatusFilter("Alive")}
            >
              <div className={styles.statusContainer}>
                <div className={styles.status}></div>
                <div style={{ textTransform: "capitalize" }}>Alive</div>
              </div>
            </button>
            <button
              className={`${styles.filter} ${statusFilter === "Unknown" ? styles.active : ""}`}
              onClick={() => setStatusFilter("unknown")}
            >
              <div className={styles.statusContainer}>
                <div className={styles.status}></div>
                <div style={{ textTransform: "capitalize" }}>Unknown</div>
              </div>
            </button>
            <button className={styles.filter} onClick={() => setStatusFilter(null)}>
              <div className={styles.statusContainer}>
                <GrPowerReset />
                <div style={{ textTransform: "capitalize" }}>Reset Filter</div>
              </div>
            </button>
          </div>
        </div>

        <Link href="/characters/favorites">My Favorites</Link>
      </div>

      {paginatedCharacters.length === 0 && !isLoading && (
        <div className={styles.notFound}>
          <img alt="" src={notFound.src} />
          <h3>There are no characters in this Location</h3>
        </div>
      )}

      <main className={classNames(styles.content, styles.web)}>
        {isLoading ? (
          <CharacterCardSkeleton />
        ) : (
          paginatedCharacters.map((character: ResultData) => (
            <CharacterCard character={character} key={character.id} />
          ))
        )}
      </main>

      <main className={styles.mobile} style={{ position: "relative" }}>
        <Slider {...sliderSettings}>
          {paginatedCharacters.map((character: ResultData) => (
            <CharacterCard character={character} key={character.id} />
          ))}
        </Slider>
      </main>

      <div className={styles.web}>
        {paginatedCharacters.length !== 0 && (
          <Pagination
            totalPageCount={totalPageCount}
            currentPage={currentPage}
            onPageChange={onPageChange}
            position="center"
          />
        )}
      </div>
    </div>
  )
}

export default CharacterPageView
