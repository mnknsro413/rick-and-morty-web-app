"use client"

import React, { FC } from "react"
import { CharactersDetailPageViewProps } from "@/module/app-characters/view/characters-detail-page/characters-detail-page.type"
import MainHeader from "@/package/layouts/main-header"
import { HiOutlineArrowCircleLeft } from "react-icons/hi"
import styles from "../../asset/style/Characters.module.scss"
import { useRouter } from "next/navigation"
import { fetchData } from "@/package/service/api"
import { ResultData } from "../../type"
import { FaRegHeart } from "react-icons/fa6"
import { useQuery } from "react-query"
import { useDispatch } from "react-redux"
import { toggleFavorite } from "../../redux/slices/favoritesSlice"
import { useAppSelector } from "../../hooks"
import { FaHeart } from "react-icons/fa"
import { useToast } from "@/core/uikit/components/tooltip/useToast"
import { ToastPosition, ToastStatus } from "@/core/uikit/constant/toast"
import { CharacterStatusColor } from "../../constant"
import Image from "next/image"
import Link from "next/link"
import classNames from "classnames"

const CharactersDetailPageView: FC<CharactersDetailPageViewProps> = (params) => {
  const router = useRouter()
  const dispatch = useDispatch()
  const showToast = useToast()

  const { data: character, isLoading } = useQuery(["character"], () =>
    fetchData(`character/${params.params}`)
  )

  const { data: suggestionCharacter, isLoading: isSuggestionLoading } = useQuery(
    ["suggestion"],
    () => fetchData(`character`)
  )

  const characterDetail = character as ResultData

  const handleFavoriteToggle = () => {
    dispatch(toggleFavorite(Number(params.params)))
    showToast({
      description: checkFavoriteCharacter ? "Added to favorites." : "Remove to favorites.",
      status: ToastStatus.Info,
      position: ToastPosition.TopRight,
      duration: 2000,
      isClosable: true,
    })
  }

  const favorites = useAppSelector((state) => state.favorites.ids)
  const checkFavoriteCharacter = !!favorites.find((id) => id === Number(params.params))

  return (
    <div>
      <header className={styles.header}>
        <MainHeader
          leftAction={
            <div className={styles.backButton} onClick={() => router.back()}>
              <HiOutlineArrowCircleLeft />
            </div>
          }
        />
      </header>
      <div className={styles.detailSection}>
        {isLoading ? (
          <div style={{ display: "flex", flexDirection: "column" }}>
            <div className={styles.skeletonImage}>
              <svg
                className={styles.skeletonImagefpo}
                width="160px"
                height="100px"
                x="50%"
                y="50%"
                viewBox="0 0 160 100"
                version="1.1"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
              >
                <path
                  d="M108.368088,36.5625 L30.8485565,36.5625 C29.319526,36.5625 28.0800018,37.8216991 28.0800018,39.375 L28.0800018,95.625 C28.0800018,97.1783009 29.319526,98.4375 30.8485565,98.4375 L108.368088,98.4375 C109.897118,98.4375 111.136642,97.1783009 111.136642,95.625 L111.136642,39.375 C111.136642,37.8216991 109.897118,36.5625 108.368088,36.5625 L108.368088,36.5625 Z M105.599533,42.1875 L105.599533,81.225 L96.7678436,68.68125 C96.2965986,68.0076728 95.5575991,67.5787153 94.747102,67.5082962 C93.936605,67.4378771 93.1366348,67.7331229 92.5596405,68.315625 L82.0668182,79.003125 L59.1154999,55.6875 C58.0356599,54.5970274 56.2916778,54.5970274 55.2118378,55.6875 L33.6171112,77.596875 L33.6171112,42.1875 L105.599533,42.1875 L105.599533,42.1875 Z M33.6171112,92.8125 L33.6171112,85.528125 L57.149826,61.621875 L80.1011444,84.9375 C81.1809844,86.0279726 82.9249665,86.0279726 84.0048065,84.9375 L94.1654022,74.64375 L105.599533,90.9 L105.599533,92.8125 L33.6171112,92.8125 L33.6171112,92.8125 Z M77.9139862,56.25 C77.9139862,53.1433983 80.3930345,50.625 83.4510956,50.625 C86.5091566,50.625 88.988205,53.1433983 88.988205,56.25 C88.988205,59.3566017 86.5091566,61.875 83.4510956,61.875 C80.3930345,61.875 77.9139862,59.3566017 77.9139862,56.25 L77.9139862,56.25 Z"
                  id="Shape"
                ></path>
              </svg>
            </div>

            <div className={styles.skeletonInfo}>
              <h2></h2>
              <p></p>
            </div>
          </div>
        ) : (
          <div className={styles.detail}>
            <div>
              <div className={styles.favoriteButton} onClick={handleFavoriteToggle}>
                {checkFavoriteCharacter ? <FaHeart /> : <FaRegHeart />}
              </div>
              <img
                className={styles.banner}
                src={characterDetail?.image}
                width="100%"
                alt={characterDetail?.name}
              />
            </div>

            <div className={styles.subCharacterInfo}>
              <h2>{characterDetail?.name}</h2>
              <div className={styles.speciesInfo}>
                <div className={styles.statusContainer}>
                  <div
                    className={styles.status}
                    style={{ backgroundColor: CharacterStatusColor[character.status] }}
                  ></div>
                  <div style={{ textTransform: "capitalize" }}>
                    {characterDetail?.status} - {characterDetail?.type}
                  </div>
                </div>
                <div>
                  {characterDetail?.species} / {characterDetail?.gender}
                </div>
              </div>
              <div className={styles.origin}>{characterDetail?.origin?.name}</div>
            </div>
          </div>
        )}

        <div className={classNames(styles.otherCharacters, styles.web)}>
          <h2>Other Characters</h2>
          <div className={styles.otherCharactersList}>
            {suggestionCharacter?.results.slice(0, 20).map((suggestion: ResultData) => (
              <Link
                style={{ textDecoration: "none" }}
                href={`/characters/detail/${suggestion.id}`}
                className={styles.otherCharactersContainer}
              >
                <div>
                  <Image width={120} height={100} src={suggestion.image} alt={suggestion.name} />
                </div>
                <div className={styles.otherCharactersInfo}>
                  <h4>{suggestion.name}</h4>
                  <b>{suggestion.location.name}</b>
                  <h5>
                    {suggestion?.species} / {suggestion?.gender}
                  </h5>
                </div>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  )
}

export default CharactersDetailPageView
