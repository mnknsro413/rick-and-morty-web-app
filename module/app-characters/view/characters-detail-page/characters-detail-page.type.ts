import { PropsWithChildren } from "react"

export type CharactersDetailPageViewProps = PropsWithChildren & {
    params?: number
}
