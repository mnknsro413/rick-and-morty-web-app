import React, { FC } from "react"
import { CharacterCardProps } from "@/module/app-characters/component/character-card"
import Link from "next/link"
import style from "../../asset/style/Characters.module.scss"
import { toggleFavorite } from "../../redux/slices/favoritesSlice"
import { useToast } from "@/core/uikit/components/tooltip/useToast"
import { useDispatch } from "react-redux"
import { useAppSelector } from "../../hooks"
import { FaHeart, FaRegHeart } from "react-icons/fa"
import {
  CharacterStatusColor,
  CharacterStatusText,
  ToastPosition,
  ToastStatus,
} from "../../constant"
import BlurryLoadingImage from "@/package/components/blue-loading-image"
import {
  getFavoritesFromLocalStorage,
  updateFavoritesInLocalStorage,
} from "../../utils/use-storage"

export const toggleFavoriteInArray = (id: number, array: number[]): number[] => {
  const index = array.indexOf(id)
  if (index !== -1) {
    // Remove from array
    array.splice(index, 1)
  } else {
    // Add to array
    array.push(id)
  }
  return array
}

const CharacterCard: FC<CharacterCardProps> = ({ character }) => {
  const showToast = useToast()
  const dispatch = useDispatch()

  const handleFavoriteToggle = () => {
    dispatch(toggleFavorite(Number(character.id)))

    const favorites = getFavoritesFromLocalStorage()
    const updatedFavorites = toggleFavoriteInArray(Number(character.id), favorites)
    updateFavoritesInLocalStorage(updatedFavorites)

    showToast({
      description: checkFavoriteCharacter ? "Added to favorites." : "Removed from favorites.",
      status: ToastStatus.Info,
      position: ToastPosition.TopRight,
      duration: 2000,
      isClosable: true,
    })
  }

  const favorites = useAppSelector((state) => state.favorites.ids)
  const checkFavoriteCharacter = !!favorites.find((id) => id === Number(character.id))

  return (
    <div className={style.container} key={character.id + 1}>
      <div className={style.favoriteButton} onClick={handleFavoriteToggle}>
        {checkFavoriteCharacter ? <FaHeart /> : <FaRegHeart />}
      </div>
      <Link href={`/characters/detail/${character.id}`} style={{ textDecoration: "none" }}>
        <BlurryLoadingImage
          alt={character.name}
          image={character.image}
          key={character.id}
          preview={character.image}
          imageStyleClass={style.banner}
        />
      </Link>
      <div className={style.contentInfo}>
        <Link href={`/characters/detail/${character.id}`} style={{ textDecoration: "none" }}>
          <h3>{character.name}</h3>
        </Link>
        <div className={style.statusContainer}>
          <div
            className={style.status}
            style={{ background: CharacterStatusColor[character.status] }}
          ></div>
          <div style={{ textTransform: "capitalize" }}>
            {`${character.status} - ${character.species}`}
          </div>
        </div>
      </div>
    </div>
  )
}

export default CharacterCard
