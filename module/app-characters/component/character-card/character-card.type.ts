import { PropsWithChildren } from "react"
import { ResultData } from "../../type"

export type CharacterCardProps = PropsWithChildren & {
    character: ResultData
}
