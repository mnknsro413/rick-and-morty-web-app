const FAVORITES_KEY = "favorites";

export const getFavoritesFromLocalStorage = (): number[] => {
    if (typeof window !== "undefined") {
        const favoritesString = localStorage.getItem(FAVORITES_KEY);
        return favoritesString ? JSON.parse(favoritesString) : [];
    } else {
        return [];
    }
};

export const updateFavoritesInLocalStorage = (favorites: number[]): void => {
    if (typeof window !== "undefined") {
        localStorage.setItem(FAVORITES_KEY, JSON.stringify(favorites));
    }
};