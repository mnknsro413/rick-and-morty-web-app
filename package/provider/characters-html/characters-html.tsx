"use client"
import { FC } from "react"

import type { CharactersHtmlProps } from "@/package/provider/characters-html/characters-html.type"

const CharactersHtml: FC<CharactersHtmlProps> = (props) => {
  const { children } = props

  return <html lang="en">{children}</html>
}

export default CharactersHtml
