"use client"
import { FC } from "react"

import type { LocationHtmlProps } from "@/package/provider/locations-html/locations-html.type"

const LocationsHtml: FC<LocationHtmlProps> = (props) => {
  const { children } = props

  return <html lang="en">{children}</html>
}

export default LocationsHtml
