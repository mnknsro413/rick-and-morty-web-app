"use client"
import { FC } from "react"

import type { LocationsBodyProps } from "@/package/provider/locations-body/locations-body.type"
import { localInter } from "@/package/constant"

const LocationsBody: FC<LocationsBodyProps> = (props) => {
  const { children } = props

  return <body className={localInter.variable}>{children}</body>
}

export default LocationsBody
