"use client"
import { FC } from "react"

import { CharactersBodyProps } from "./characters-body.type"

const CharactersBody: FC<CharactersBodyProps> = (props) => {
  const { children } = props

  return <body>{children}</body>
}

export default CharactersBody
