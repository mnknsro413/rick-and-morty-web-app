const BASE_URL = process.env.NEXT_PUBLIC_API_BASE_URL || "https://rickandmortyapi.com/api/";

export const fetchData = async (endpoint: string) => {
    try {
        const response = await fetch(`${BASE_URL}${endpoint}`);
        if (!response.ok) {
            throw new Error("Failed to fetch data");
        }
        return response.json();
    } catch (error: any) {
        throw new Error(`Error: ${(error as Error).message}`);
    }
};