import type { FC } from "react"
import React from "react"
import { MainHeaderProps } from "@/package/layouts/main-header/main-header.type"
import Image from "next/image"
import logo from "@/package/asset/images/logo.svg"
import styles from "./style/Header.module.scss"
import Link from "next/link"

const MainHeader: FC<MainHeaderProps> = ({ leftAction, rightAction }) => {
  return (
    <nav className={styles.container}>
      <div>{leftAction}</div>
      <Link href="/">
        <Image width={250} height={60} src={logo} alt="Rick and Morty" />
      </Link>
      <div>{rightAction}</div>
    </nav>
  )
}

export default MainHeader
