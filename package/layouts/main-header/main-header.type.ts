import { PropsWithChildren, ReactNode } from 'react';

export type MainHeaderProps = PropsWithChildren & {
    leftAction?: ReactNode
    rightAction?: ReactNode
};
