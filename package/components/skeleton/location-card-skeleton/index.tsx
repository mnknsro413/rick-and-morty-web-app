import React from "react"
import style from "../Loading.module.scss"
import classNames from "classnames"

const LocationCardSkeleton = () => {
  return (
    <>
      {[...Array(12).keys()].map((i) => (
        <div className={style.cards} key={i}>
          <div className={classNames(style.card, style.isLoading)}>
            <div className={style.content}>
              <h2></h2>
            </div>
            <div className={style.content}>
              <h2></h2>
            </div>
          </div>
        </div>
      ))}
    </>
  )
}

export default LocationCardSkeleton
