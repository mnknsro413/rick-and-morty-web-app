import React from "react"
import styles from "./style/Loading.module.scss"
import Image from "next/image"
import logo from "@/package/asset/gif/loading.gif"
const Loading = () => {
  return (
    <div className={styles.container}>
      <Image src={logo} width={150} alt="Rick and Morth Loading" />
      Loading...
    </div>
  )
}

export default Loading
