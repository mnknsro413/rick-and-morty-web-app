import Link from "next/link"
import { NextPage } from "next"
import styles from "../style/Error.module.scss"
import Image from "next/image"
import notFound from "@/package/asset/images/page-not-found.webp"

const NotFoundPage: NextPage = () => {
  return (
    <div className={styles.global}>
      <Image
        className={styles.banner}
        alt="Not found gif"
        src={notFound}
        width={400}
        height={300}
      />
      <h2>Not Found</h2>
      <p>Could not find requested resource</p>
      <Link href="/">Return Home</Link>
    </div>
  )
}

export default NotFoundPage
