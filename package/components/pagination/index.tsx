import React, { FC } from "react"
import styles from "./style/Pagination.module.scss"
import classnames from "classnames"

interface PaginationProps {
  totalPageCount: number
  currentPage: number
  onPageChange: (page: number) => void
  position: "left" | "center" | "right"
}

const Pagination: FC<PaginationProps> = ({
  totalPageCount,
  currentPage,
  onPageChange,
  position = "center",
}) => {

  const renderPageButtons = () => {
    const buttons = []
    const maxAdjacentPages = 1

    let start = Math.max(1, currentPage - maxAdjacentPages)
    let end = Math.min(currentPage + maxAdjacentPages, totalPageCount)

    if (currentPage - start < maxAdjacentPages) {
      end += Math.min(maxAdjacentPages - (currentPage - start), totalPageCount - end)
    }

    if (end - currentPage < maxAdjacentPages) {
      start -= Math.min(maxAdjacentPages - (end - currentPage), start - 1)
    }

    if (start > 1) {
      buttons.push(
        <button className={styles.button} key={1} onClick={() => onPageChange(1)}>
          1
        </button>
      )
      if (start > 2) {
        buttons.push(
          <span key={0} className={styles.ellipsis}>
            ...
          </span>
        )
      }
    }

    for (let i = start; i <= end; i++) {
      buttons.push(
        <button
          key={i}
          className={classnames(styles.button, { [styles.active]: i === currentPage })}
          onClick={() => onPageChange(i)}
        >
          {i}
        </button>
      )
    }

    if (end < totalPageCount) {
      if (totalPageCount - end > 1) {
        buttons.push(
          <span key={totalPageCount - 1} className={styles.ellipsis}>
            ...
          </span>
        )
      }
      buttons.push(
        <button
          className={styles.button}
          key={totalPageCount}
          onClick={() => onPageChange(totalPageCount)}
        >
          {totalPageCount}
        </button>
      )
    }

    return buttons
  }

  const positionControl = (position: string) => {
    switch (position) {
      case "left":
        return styles.left
      case "center":
        return styles.center
      case "right":
        return styles.right
      default:
        return styles.center
    }
  }

  return (
    <div className={classnames(positionControl(position), styles.pagination)}>
      <button
        className={styles.button}
        onClick={() => onPageChange(currentPage - 1)}
        disabled={currentPage === 1}
      >
        {"<"}
      </button>
      {renderPageButtons()}
      <button
        className={styles.button}
        onClick={() => onPageChange(currentPage + 1)}
        disabled={currentPage === totalPageCount}
      >
        {">"}
      </button>
    </div>
  )
}

export default Pagination
