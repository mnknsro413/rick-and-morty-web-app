## Rick and Morty App - Muhsin Deniz

Next JS version: 14.0.4

Node JS version: 18.18.0

You can access the live version of the project [https://muhsin-rick-and-morty-app.vercel.app/](https://muhsin-rick-and-morty-app.vercel.app/).

### Dependencies used:

For State management and Fetch:

```bash
- Redux Toolkit
- ReduxThunk
- Tanstack React Query
```

For styling:

```bash
- SASS
```

For Standard Code Formatting and Problem analysis:

```bash
- Eslint
- Prettier
```

For icons:

```bash
- React Icons
```

For Mobile Carousel Structure:

```bash
- Slick Carousel
```
