import LocationCardSkeleton from "@/package/components/skeleton/location-card-skeleton"

export default function Loading() {
  return <LocationCardSkeleton />
}
