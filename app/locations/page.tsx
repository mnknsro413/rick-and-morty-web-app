"use client"

import LocationsPageView from "@/module/app-locations/view/locations-page/locations-page"
import Head from "next/head"

import React from "react"
import { QueryClient, QueryClientProvider } from "react-query"

const queryClient = new QueryClient()

export default function Page() {
  return (
    <>
      <Head>
        <title>Rick and Morty | Muhsin Deniz</title>
        <meta name="description" content="Learn about Rick and Morty locations." />
        <meta property="og:title" content="Rick and Morty Wiki" />
        <meta property="og:description" content="Learn about Rick and Morty location planets." />
        <meta
          property="og:image"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
        <meta property="og:url" content="https://muhsin-rick-and-morty-app.vercel.app/" />
        <meta
          name="twitter:card"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
      </Head>
      <QueryClientProvider client={queryClient}>
        <LocationsPageView />
      </QueryClientProvider>
    </>
  )
}
