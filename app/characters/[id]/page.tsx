"use client"

import CharacterPageView from "@/module/app-characters/view/characters-page/characters-page"
import Head from "next/head"

export default function Page({ params }: { params: { id: string } }) {
  return (
    <>
      <Head>
        <title>Rick and Morty Character Detail Page</title>
        <meta name="description" content="Learn about Rick and Morty character detail page." />
        <meta property="og:title" content="Rick and Morty Character Detail Page" />
        <meta
          property="og:description"
          content="Learn about Rick and Morty character detail page."
        />
        <meta
          property="og:image"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
        <meta property="og:url" content="https://muhsin-rick-and-morty-app.vercel.app/" />
        <meta
          name="twitter:card"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
      </Head>
      <CharacterPageView params={params.id} />
    </>
  )
}
