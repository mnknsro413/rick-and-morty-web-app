"use client"

import FavoriteCharactersPageView from "@/module/app-characters/view/favorite-characters-page/favorite-characters-page"
import Head from "next/head"
import React from "react"

export default function Page() {
  return (
    <>
      <Head>
        <title>Favorite Characters</title>
        <meta name="description" content="Learn about Rick and Morty my favorite characters." />
        <meta property="og:title" content="Rick and Morty my favorite characters." />
        <meta property="og:description" content="Rick and Morty my favorite characters." />
        <meta
          property="og:image"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
        <meta property="og:url" content="https://muhsin-rick-and-morty-app.vercel.app/" />
        <meta
          name="twitter:card"
          content="https://muhsin-rick-and-morty-app.vercel.app/_next/static/media/logo.836f2487.svg"
        />
      </Head>
      <FavoriteCharactersPageView />
    </>
  )
}
