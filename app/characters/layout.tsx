"use client"

import React, { FC, PropsWithChildren } from "react"
import { QueryClient, QueryClientProvider } from "react-query"

type RootLayoutProps = PropsWithChildren<{}>

const queryClient = new QueryClient()

const CharactersLayout: FC<RootLayoutProps> = (props) => {
  const { children } = props

  return <QueryClientProvider client={queryClient}>{children}</QueryClientProvider>
}

export default CharactersLayout
