import NotFoundMessage from "@/package/components/error/not-found"

export default function NotFound() {
  return <NotFoundMessage />
}
