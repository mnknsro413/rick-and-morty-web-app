"use client"

import { Provider } from "react-redux"

import "@/package/asset/style/global.scss"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

import React, { FC, PropsWithChildren } from "react"

import { store } from "@/module/app-characters/store"
import { ToastProvider } from "@/core/uikit/components/tooltip/useToast"

type RootLayoutProps = PropsWithChildren

const RootLayout: FC<RootLayoutProps> = (props) => {
  const { children } = props

  return (
    <html>
      <body>
        <Provider store={store}>
          <ToastProvider>{children}</ToastProvider>
        </Provider>
      </body>
    </html>
  )
}

export default RootLayout
